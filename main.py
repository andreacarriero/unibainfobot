#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, Job
from telegram.ext import MessageHandler, Filters
import logging, datetime, string
import json
import feedparser

# Loading config file
with open('data/conf.json') as config_file:
    config = json.load(config_file)


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)
log = logging

def parse_news(channel_n):
    log.info("Invoking feed parser for %s" % (config['channels'][channel_n]['name']))
    parser = feedparser.parse(config['channels'][channel_n]['rss_feed_url'])
    log.info("Feed parsed")

    # Build news dict list
    log.info("Building news dict")
    news = []
    for item in parser['entries']:
        info = {
            'title': item['title'],
            'date': item['updated'],
            'link': item['link']
        }
        news.append(info)
    return news

def filter_news(news_list, channel_n):
    log.info("Filtering news for %s" % (config['channels'][channel_n]['name']))
    new_list = []
    for item in news_list:
        title = item['title']
        for filter_str in config['channels'][channel_n]['filters']:
            for char in string.punctuation:
                title.replace(char, '')
        if filter_str.upper() in title.upper():
            new_list.append(item)
    tot_news_counter = len(news_list)
    filtered_news_counter = len(new_list)
    log.info("%s news filtered out of %s" % (filtered_news_counter, tot_news_counter))
    return new_list

def format_rss_date(date):
    date = date.replace('Z', '').split('T')
    
    hour = date[1].split(':'); hour = ':'.join([hour[0], hour[1]])
    d_m_y = date[0].split('-'); d_m_y = '/'.join(d_m_y[::-1])

    formatted_date_str = ("%s - %s" % (d_m_y, hour))
    return formatted_date_str

def build_message(news_item):
    log.info("Building message for news item")
    message = ""
    message = message + "<b>" + news_item['title'] + "</b>\n"
    message = message + format_rss_date(news_item['date']) + "\n"
    message = message + "<a href='" + news_item['link'] + "'>Leggi di più</a>"
    return message

def send_once(news_list, channel_n):
    log.info("Loading cache file for %s" % (config['channels'][channel_n]['name']))
    cache_file_name = 'data/cache_' + config['channels'][channel_n]['id'] + '.json'
    try:
        with open(cache_file_name) as cache_file:
            log.info("Loaded cache file")
            cache = json.load(cache_file)
            local_copy = cache['cache']
    except FileNotFoundError:
        log.info("Cache file not found, creating an empty one")
        with open(cache_file_name, 'w') as cache_file:
            json.dump({'cache': []}, cache_file)
            local_copy = []


    if len(local_copy) == 0:
        log.info("Local news copy is empty")
        log.info("Initializing local news copy")
        local_copy = news_list

        with open(cache_file_name, 'w') as cache_file:
            json.dump({'cache': local_copy}, cache_file)

        return news_list

    else:
        log.info("Local news copy is not empty")
        tmp_news_list = []
        for item in news_list:
            if not item in local_copy:
                tmp_news_list.append(item)
                local_copy.append(item)

                with open(cache_file_name, 'w') as cache_file:
                    json.dump({'cache': local_copy}, cache_file)

        return tmp_news_list

def loop(bot, update):
    log.info("Main job execution")
    for channel_n, channel in enumerate(config['channels']):
        news = filter_news(send_once(parse_news(channel_n), channel_n), channel_n)
        for item in news:
            log.info("Sending news %s to %s" % (item['title'], channel['name']))
            bot.sendMessage(
                channel['id'],
                text=build_message(item),
                parse_mode='HTML',
                disable_web_page_preview=True
                )

def start(bot, update, args, job_queue, chat_data):
    chat_id = update.message.chat_id
    if str(chat_id) == str(config['admin_id']):
        log.info("Starting new session with %s", chat_id)

        log.info("Setting up main job for %s", chat_id)
        job = job_queue.run_repeating(loop, config['check_delay'], context=chat_id)
        chat_data['job'] = job
        job_queue.start()

        bot.sendMessage(chat_id, text="Bot started!")
    else:
        log.error("%s Unauthorized to run start command" % (chat_id))
        bot.sendMessage(chat_id, text="Unauthorized")

def stop(bot, update, args, job_queue, chat_data):
    chat_id = update.message.chat_id
    if str(chat_id) == str(config['admin_id']):
        log.info("Stopping main job")
        job_queue.stop()
        bot.sendMessage(chat_id, text="Bot stopped!")
    else:
        log.error("%s Unauthorized to run stop command" % (chat_id))
        bot.sendMessage(chat_id, text="Unauthorized")

def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))

def main():
    updater = Updater(config['api_token'])
    dp = updater.dispatcher
    
    # Error handlers
    dp.add_error_handler(error)
    
    # Command handlers
    dp.add_handler(CommandHandler("start", start, pass_args=True, pass_job_queue=True, pass_chat_data=True))
    dp.add_handler(CommandHandler("stop", stop, pass_args=True, pass_job_queue=True, pass_chat_data=True))

    # Start the Bot
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()