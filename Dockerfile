FROM python:3-alpine
LABEL maintainer="info@andreacarriero.com"
WORKDIR /usr/src/app
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
VOLUME ["./data"]
CMD [ "python", "./main.py" ]