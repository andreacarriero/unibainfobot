#Uniba Info Bot

##How to set dev environment
1. Install `python`, `python-pip` and `virtualenv` on your device
2. Clone git repository
3. `cd` in project directory
4. Build virtual environment with `virtualenv venv -p python3`
5. Activate virtual env with `python venv/bin/activate`
6. Install requirements with `pip install -r requirements.txt`
7. Run main file with `python main.py`

##What to do before runnig
1. Copy `conf.json.example` to `conf.json`
2. Insert personal data to predefined fields in `conf.json`